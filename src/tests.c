#include "tests.h"

#include <assert.h>

#include "mem.h"
#include "mem_internals.h"

void debug(const char *fmt, ...);

struct block_header *block_get_header(void *contents)
{
    return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

#define HEAP_SIZE 8000

int run_tests()
{
    {
        debug("Test 1");
        void *heap = heap_init(HEAP_SIZE);
        assert(heap && "heap_init() failed");
        puts("Initial heap status:");
        debug_heap(stdout, heap);

        void *ptr = _malloc(25);
        assert(ptr && "_malloc() failed");
        puts("\nHeap after malloc:");
        debug_heap(stdout, heap);

        _free(ptr);
        heap_term();
    }
    {
        debug("Test 2");

        void *heap = heap_init(HEAP_SIZE);
        assert(heap && "heap_init() failed");
        puts("Initial heap status:");
        debug_heap(stdout, heap);

        void *ptr1 = _malloc(25);
        assert(ptr1 && "_malloc() failed");

        void *ptr2 = _malloc(125);
        assert(ptr2 && "_malloc() failed");

        void *ptr3 = _malloc(40);
        assert(ptr3 && "_malloc() failed");

        puts("\nHeap after allocation of 3 blocks:");
        debug_heap(stdout, heap);

        _free(ptr2);
        puts("\nHeap after free second block:");
        debug_heap(stdout, heap);

        _free(ptr1);
        _free(ptr3);
        heap_term();
    }
    {
        debug("Test 3");
        void *heap = heap_init(HEAP_SIZE);
        assert(heap && "heap_init() failed");
        puts("Initial heap status:");
        debug_heap(stdout, heap);

        void *ptr1 = _malloc(100);
        assert(ptr1 && "_malloc() failed");

        void *ptr2 = _malloc(24);
        assert(ptr2 && "_malloc() failed");

        void *ptr3 = _malloc(48);
        assert(ptr3 && "_malloc() failed");

        puts("\nHeap after allocation of 3 blocks:");
        debug_heap(stdout, heap);

        _free(ptr3);
        _free(ptr2);
        puts("\nHeap after free 2 blocks:");
        debug_heap(stdout, heap);

        _free(ptr1);
        heap_term();
    }
    {
        debug("Test 4");
        void *heap = heap_init(HEAP_SIZE);
        assert(heap && "heap_init() failed");
        puts("Initial heap status:");
        debug_heap(stdout, heap);

        void *ptr1 = _malloc(8000);
        assert(ptr1 && "_malloc() failed");

        puts("\nHeap after the memory has almost run out:");
        debug_heap(stdout, heap);

        void *ptr2 = _malloc(HEAP_SIZE);
        assert(ptr2 && "_malloc() failed");

        puts("\nHeap extended after new huge allocation:");
        debug_heap(stdout, heap);

        _free(ptr2);
        _free(ptr1);
        heap_term();
    }

    return 0;
}
